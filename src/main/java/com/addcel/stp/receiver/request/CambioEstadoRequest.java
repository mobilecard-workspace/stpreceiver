/**
 * @author Victor Ramirez
 */

package com.addcel.stp.receiver.request;

import lombok.Data;

@Data
public class CambioEstadoRequest {
	
	private Long id;
	private String empresa;
	private String folioOrigen;
	private String estado;
	private Integer causaDevolucion;
	
}
