/**
 * @author Victor Ramirez
 */

package com.addcel.stp.receiver.request;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;

@Data
public class NotificarPagoRequest {

	@NotEmpty
	private String id;
	
	private Integer fecha;
	
	private Integer institucionOrdenante;
	
	private Integer institucionBeneficiaria;
	
	@NotEmpty
	private String claveRastreo;
	
	private Double monto;
	
	@NotEmpty
	private String nombreOrdenante;
	
	@NotEmpty
	private String cuentaOrdenante;
	
	@NotEmpty
	private String nombreBeneficiario;
	
	private Integer tipoCuentaBeneficiario;
	
	@NotEmpty
	@Size(min = 18, max = 18)
	private String cuentaBeneficiario;
	
	@NotEmpty
	private String rfcCurpBeneficiario;
	
	@NotEmpty
	private String conceptoPago;
	
	@NotEmpty
	private String referenciaNumerica;
	
	@NotEmpty
	private String empresa;
	
}
