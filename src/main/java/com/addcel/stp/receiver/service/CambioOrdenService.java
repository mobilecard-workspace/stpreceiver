/**
 * @author Victor Ramirez
 */

package com.addcel.stp.receiver.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.stp.receiver.constants.StatusConstants;
import com.addcel.stp.receiver.domain.StpTransaccionesDispersion;
import com.addcel.stp.receiver.repository.StpTransaccionesDispersionRepository;
import com.addcel.stp.receiver.request.CambioEstadoRequest;
import com.addcel.stp.receiver.response.ApiResponse;

@Service
public class CambioOrdenService {

	@Autowired
	private StpTransaccionesDispersionRepository stpTransaccionesRepo;
	
	private static final Logger LOGGER = LogManager.getLogger(CambioOrdenService.class);
	
	public ApiResponse cambiarEstado(CambioEstadoRequest cambioEstadoReq) {
		ApiResponse apiResp = new ApiResponse();
		
		LOGGER.debug("Cambio de Estado Request: " + cambioEstadoReq.toString());
		
		if(cambioEstadoReq.getCausaDevolucion() == 0) {
			LOGGER.info("El pago fue exitoso");
		} else {
			LOGGER.warn("El pago fue devuelto, id de la causa devolucion " + cambioEstadoReq.getCausaDevolucion());
		}
		
		LOGGER.debug("Buscando transaccion en la BD por el idOrden: " + cambioEstadoReq.getId());
		StpTransaccionesDispersion stpTransaccion = stpTransaccionesRepo.findByIdOrden(cambioEstadoReq.getId());
		
		if(stpTransaccion != null) {
			LOGGER.debug("Transaccion encontrada: " + stpTransaccion.toString());
						
			stpTransaccion.setEstado(cambioEstadoReq.getEstado());
			stpTransaccion.setIdDevolucion(cambioEstadoReq.getCausaDevolucion());
			
			LOGGER.debug("Actualizando el estado de la transaccion ...");
			StpTransaccionesDispersion stpTransaccionSaved = stpTransaccionesRepo.save(stpTransaccion);
			LOGGER.info("Transaccion actualizada correctamente: " + stpTransaccionSaved.toString());
			
			apiResp.setCode(StatusConstants.SUCCESS_CODE);
			apiResp.setMessage(StatusConstants.SUCCESS_MSG);
		} else {
			apiResp.setCode(StatusConstants.IDORDEN_NOT_FOUND_CODE);
			apiResp.setMessage(StatusConstants.IDORDEN_NOT_FOUND_MSG);
			LOGGER.error("Transaccion no encontrada en la BD con ese ID");
		}
		
		return apiResp;
	}
	
}
