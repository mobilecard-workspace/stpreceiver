/**
 * @author Victor Ramirez
 */

package com.addcel.stp.receiver.service;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.stp.receiver.constants.MobileCardConstants;
import com.addcel.stp.receiver.constants.SMSConstants;
import com.addcel.stp.receiver.constants.StatusConstants;
import com.addcel.stp.receiver.domain.LcpfEstablecimiento;
import com.addcel.stp.receiver.domain.PreviValeCardStock;
import com.addcel.stp.receiver.domain.StpCuentasClabe;
import com.addcel.stp.receiver.domain.StpTransaccionesCobranza;
import com.addcel.stp.receiver.domain.TBitacora;
import com.addcel.stp.receiver.domain.TUsuarios;
import com.addcel.stp.receiver.repository.LcpfEstableciemientoRepository;
import com.addcel.stp.receiver.repository.PreviValeCardStockRepository;
import com.addcel.stp.receiver.repository.StpCuentasClabeRepository;
import com.addcel.stp.receiver.repository.StpTransaccionesCobranzaRepository;
import com.addcel.stp.receiver.repository.TBitacorasRepository;
import com.addcel.stp.receiver.repository.TUsuariosRepository;
import com.addcel.stp.receiver.request.NotificarPagoRequest;
import com.addcel.stp.receiver.response.ApiResponse;
import com.addcel.stp.receiver.util.PropertiesFile;
import com.addcel.stp.receiver.util.RestClient;
import com.addcel.stp.receiver.ws.request.AgregarDineroRequest;
import com.addcel.stp.receiver.ws.request.SMSParams;
import com.addcel.stp.receiver.ws.request.SendSMSRequest;
import com.addcel.stp.receiver.ws.response.BaseResponse;
import com.addcel.utils.AddcelCrypto;

@Service
public class NotificarPagoService {

	private static final Logger LOGGER = LogManager.getLogger(NotificarPagoService.class);
	
	@Autowired
	private RestClient restClient;
	
	@Autowired
	private StpCuentasClabeRepository stpCuentasClabeRepo;
	
	@Autowired
	private StpTransaccionesCobranzaRepository stpTransaccionesCobranzaRepo;
	
	@Autowired
	private PreviValeCardStockRepository previValeCardStockRepo;
	
	@Autowired
	private TUsuariosRepository usuariosRepo;
	
	@Autowired
	private LcpfEstableciemientoRepository establecimientoRepo;
	
	@Autowired
	private TBitacorasRepository tBitacorasRepo;
	
	@Autowired
	private PropertiesFile propsFile;
	
	public ApiResponse notificarPago(NotificarPagoRequest notificarPagoReq) {
		ApiResponse apiResp = new ApiResponse();
		apiResp.setCode(StatusConstants.COBRANZA_ERROR_CODE);
		apiResp.setMessage(StatusConstants.COBRANZA_ERROR_MSG);
		
		LOGGER.debug("Notificar Pago Request: " + notificarPagoReq.toString());
		
		LOGGER.debug("Buscando cuenta CLABE: " + notificarPagoReq.getCuentaBeneficiario());
		StpCuentasClabe stpCuentasClabe = stpCuentasClabeRepo.findByClabe(notificarPagoReq.getCuentaBeneficiario());
		
		if(stpCuentasClabe != null && stpCuentasClabe.getClabe().equals(propsFile.getStpClabeMaestra())) {
			LOGGER.info("Deposito recibido para la CLABE Maestra, se finaliza el flujo");
			apiResp.setCode(StatusConstants.COBRANZA_SUCCESS_CODE);
			apiResp.setMessage(StatusConstants.COBRANZA_SUCCESS_MSG);
			
			return apiResp;
		}
		
		if(stpCuentasClabe != null && stpCuentasClabe.getActiva() == MobileCardConstants.CLABE_ACTIVA) {
			LOGGER.info("Cuenta CLABE encontrada: " + stpCuentasClabe.toString());
			
			LOGGER.debug("Buscando una tarjeta de MobileCard que tenga asignada esa CLABE y que este activa ...");
			PreviValeCardStock previValeCardStock = previValeCardStockRepo.findByIdStpCuentasClabe(stpCuentasClabe.getIdStpCuentasClabe());
			
			if(previValeCardStock != null && previValeCardStock.getActiva() == MobileCardConstants.PREVIVALE_CARD_ACTIVA) {
				LOGGER.info("Tarjeta activa de MobileCard que tiene esa CLABE asignada: " + previValeCardStock.getUltimosNumeros());
				
				AgregarDineroRequest agregarDineroReq = new AgregarDineroRequest();
				agregarDineroReq.setTarjeta(previValeCardStock.getNumero());
				agregarDineroReq.setCantidad(notificarPagoReq.getMonto());
				
				TUsuarios usuario = null;
				LcpfEstablecimiento establecimiento = null;
				Long idUsuario = null;
				int idTarjeta = 0;
				String nombre = "";
				String telefono = "";
				String tarjeta = AddcelCrypto.decryptTarjeta(previValeCardStock.getNumero());
				tarjeta = tarjeta.substring(0, 3) + "********" + previValeCardStock.getUltimosNumeros();
				
				if(previValeCardStock.getIdUsuario() != null) {
					agregarDineroReq.setIdUsuario(previValeCardStock.getIdUsuario());
					usuario = usuariosRepo.findOne(previValeCardStock.getIdUsuario());
					nombre = usuario.getUsrNombre() + " " + usuario.getUsrApellido();
					telefono = usuario.getUsrTelefono();
					idUsuario = usuario.getIdUsuario();
					idTarjeta = previValeCardStock.getIdTarjetasUsuario();
					
					LOGGER.info("Agregando dinero a la tarjeta del usuario: " + nombre);
				} else if(previValeCardStock.getIdEstablecimiento() != null) {
					agregarDineroReq.setIdEstablecimiento(previValeCardStock.getIdEstablecimiento());
					establecimiento = establecimientoRepo.findOne(previValeCardStock.getIdEstablecimiento());
					nombre = establecimiento.getNombreEstablecimiento();
					telefono = establecimiento.getTelefonoContacto();
					idUsuario = establecimiento.getId();
					idTarjeta = previValeCardStock.getIdTarjetasEstablecimiento().intValue();
					
					LOGGER.info("Agregando dinero a la tarjeta del establecimiento: " + nombre);
				}
				
				boolean success = agregarDineroPV(notificarPagoReq, agregarDineroReq, apiResp, previValeCardStock.getIdPrevivaleCardStock());
			
				if(success) {
					enviarSMS("", telefono, tarjeta, String.valueOf(notificarPagoReq.getMonto()), nombre);
					
					TBitacora bitacora = new TBitacora();
					bitacora.setBitCardId(idTarjeta);
					bitacora.setBitCargo(BigDecimal.valueOf(notificarPagoReq.getMonto()));
					bitacora.setBitComision(new BigDecimal(0.0));
					bitacora.setBitConcepto(propsFile.getBitacoraConcepto());
					bitacora.setBitFecha(new Date());
					bitacora.setBitHora(new Date());
					bitacora.setBitNoAutorizacion(null);
					bitacora.setBitStatus(propsFile.getBitacoraStatus());
					bitacora.setBitTicket(notificarPagoReq.getNombreOrdenante() + " - " + notificarPagoReq.getInstitucionOrdenante());
					bitacora.setIdAplicacion(propsFile.getBitacoraIdAplicacion());
					bitacora.setIdioma(propsFile.getBitacoraIdioma());
					bitacora.setIdProducto(propsFile.getBitacoraIdProducto());
					bitacora.setIdProveedor(propsFile.getBitacoraIdProveedor());
					bitacora.setIdUsuario(idUsuario);
					bitacora.setTarjetaCompra(previValeCardStock.getNumero());
					
					try {
						LOGGER.debug("Guardando en BD la bitacora ...");
						TBitacora bitacoraSaved = tBitacorasRepo.save(bitacora);
						LOGGER.info("Se ha persistido correctamente la bitacora: " + bitacoraSaved.toString());
					} catch(Exception ex) {
						LOGGER.error("Error al persistir la bitacora: " + ex.getMessage());
						LOGGER.warn("No se pudo persistir en BD la bitacora: " + bitacora.toString());
						ex.printStackTrace();
					}
				}
			} else {
				LOGGER.warn("No hay ninguna tarjeta de MobileCard que tenga asignada esa CLABE o la tarjeta se encuentra inactiva");
			}
		} else {
			LOGGER.warn("No se encontro esa cuenta clabe en la BD de MobileCard o se encuentra inactiva");
		}
		
		return apiResp;
	}
	
	public boolean agregarDineroPV(NotificarPagoRequest notificarPagoReq, AgregarDineroRequest agregarDineroReq, 
			ApiResponse apiResp, long idPrevivaleCardStock) {
		boolean success = false;
		
		try {
			BaseResponse agregarDineroResp = restClient.createRequest(propsFile.getPrevivaleServiceUrl(), agregarDineroReq, MobileCardConstants.REST_PV);
			
			if(agregarDineroResp.getCode() == MobileCardConstants.PREVIVALE_SUCCESS) {
				LOGGER.info("Se ha agregado correctamente el dinero a la tarjeta");
				success = true;
				
				apiResp.setCode(StatusConstants.COBRANZA_SUCCESS_CODE);
				apiResp.setMessage(StatusConstants.COBRANZA_SUCCESS_MSG);
				
				StpTransaccionesCobranza stptransaccionesCobranza = new StpTransaccionesCobranza();
				stptransaccionesCobranza.setClaveRastreo(notificarPagoReq.getClaveRastreo());
				stptransaccionesCobranza.setConceptoPago(notificarPagoReq.getConceptoPago());
				stptransaccionesCobranza.setCuentaBeneficiario(notificarPagoReq.getCuentaBeneficiario());
				stptransaccionesCobranza.setCuentaOrdenante(notificarPagoReq.getCuentaOrdenante());
				stptransaccionesCobranza.setEmpresa(notificarPagoReq.getEmpresa());
				stptransaccionesCobranza.setFecha(notificarPagoReq.getFecha());
				stptransaccionesCobranza.setIdOrden(notificarPagoReq.getId());
				stptransaccionesCobranza.setIdPrevivaleCardStock(idPrevivaleCardStock);
				stptransaccionesCobranza.setInstitucionBeneficiaria(notificarPagoReq.getInstitucionBeneficiaria());
				stptransaccionesCobranza.setInstitucionOrdenante(notificarPagoReq.getInstitucionOrdenante());
				stptransaccionesCobranza.setMonto(notificarPagoReq.getMonto());
				stptransaccionesCobranza.setNombreBeneficiario(notificarPagoReq.getNombreBeneficiario());
				stptransaccionesCobranza.setNombreOrdenante(notificarPagoReq.getNombreOrdenante());
				stptransaccionesCobranza.setReferenciaNumerica(notificarPagoReq.getReferenciaNumerica());
				stptransaccionesCobranza.setRfcCurpBeneficiario(notificarPagoReq.getRfcCurpBeneficiario());
				stptransaccionesCobranza.setTipoCuentaBeneficiario(notificarPagoReq.getTipoCuentaBeneficiario());
				
				LOGGER.debug("Persistiendo en la BD la transaccion de cobranza");
				StpTransaccionesCobranza stptransaccionesCobranzaSaved = stpTransaccionesCobranzaRepo.save(stptransaccionesCobranza);
				LOGGER.debug("Transaccion de cobranza persistida en BD: " + stptransaccionesCobranzaSaved.toString());
			} else {
				LOGGER.error("El servicio de PreviVale no pudo agregar el dinero a la tarjeta");
			}
		} catch (Exception ex) {
			LOGGER.error("Ocurrio un error con el WS de PreviVale al agregar el dinero a la tarjeta: " + ex.getMessage());
			ex.printStackTrace();
		}
		
		return success;
	}
	
	public void enviarSMS(String idUsuario, String numeroCelular, String cuenta, String monto, String nombre) {
		SendSMSRequest sendSmsReq = new SendSMSRequest();
		sendSmsReq.setIdMensaje(SMSConstants.PV_ID_MSG);
		sendSmsReq.setIdUsuario(idUsuario);
		sendSmsReq.setNumeroCelular(numeroCelular);
		
		SMSParams params = new SMSParams();
		params.setCuenta(cuenta);
		params.setMonto(monto);
		params.setNombre(nombre);
		sendSmsReq.setParams(params);
		
		try {
			LOGGER.debug("Enviando SMS de notificacion al celular: " + numeroCelular);
			BaseResponse response = restClient.createRequest(propsFile.getSmsServiceUrl(), sendSmsReq, MobileCardConstants.REST_SMS);
			
			if(response != null && response.getCode() == SMSConstants.SMS_SUCCESS_CODE) {
				LOGGER.info("Se envio correctamente el SMS de notificacion");
			} else {
				LOGGER.warn("La API de SMS no pudo enviar la notificacion: " + response.getMessage());
			}
		} catch(Exception ex) {
			LOGGER.error("Ocurrio un error al consumir la API de SMS: " + ex.getMessage());
			ex.printStackTrace();
		}
	}
	
}
