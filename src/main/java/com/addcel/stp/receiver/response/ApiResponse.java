/**
 * @author Victor Ramirez
 */

package com.addcel.stp.receiver.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponse {

	private Integer code;
	private String message;
	
}
