/**
 * @author Victor Ramirez
 */

package com.addcel.stp.receiver.constants;

public class StatusConstants {

	//Codigos de error
	public static final int SUCCESS_CODE = 1000;
	public static final int IDORDEN_NOT_FOUND_CODE = 2000;
	public static final int INTERNAL_SERVER_ERROR_CODE = 5000;
	
	public static final int COBRANZA_SUCCESS_CODE = 0;
	public static final int COBRANZA_ERROR_CODE = -1;
	
	//Mensajes de error
	public static final String SUCCESS_MSG = "Se cambio el estado de la orden correctamente";
	public static final String IDORDEN_NOT_FOUND_MSG = "No se encontro esa orden en la BD de ADDCEL";
	public static final String INTERNAL_SERVER_ERROR_MSG = "Ocurrio un error interno del servidor";
	
	public static final String COBRANZA_SUCCESS_MSG = "CLABE encontrada correctamente";
	public static final String COBRANZA_ERROR_MSG = "La CLABE no esta asignada a MobileCard";
	
}
