/**
 * @author Victor Ramirez
 */

package com.addcel.stp.receiver.constants;

public class MobileCardConstants {
	
	public static final int USUARIO_ACTIVO = 1;
	public static final int USUARIO_ACTIVO_2 = 99;
	public static final int USUARIO_BLOQUEADO = 3;
	public static final int USUARIO_RESET = 98;
	public static final char USUARIO_WS_ACTIVO = 'T';
	public static final int CLABE_ACTIVA = 1;
	public static final int CLABE_INACTIVA = 0;
	public static final char PREVIVALE_CARD_ACTIVA = 'Y';
	public static final char PREVIVALE_CARD_INACTIVA = 'N';
	public static final int PREVIVALE_SUCCESS = 1000;
	
	public static final int REST_PV = 1;
	public static final int REST_SMS = 2;
	
}
