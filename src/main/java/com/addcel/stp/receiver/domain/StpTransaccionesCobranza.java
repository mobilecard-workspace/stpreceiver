/**
 * @author Victor Ramirez
 */

package com.addcel.stp.receiver.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "stp_transacciones_cobranza")
public class StpTransaccionesCobranza {

	@Id
	@Column(name = "id_stp_transacciones_cobranza", nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long idStpTransaccionesDispersion;
	
	@Column(name = "id_orden", nullable = false)
	private String idOrden;
	
	@Column(name = "fecha", nullable = false)
	private Integer fecha;
	
	@Column(name = "institucion_ordenante", nullable = false)
	private Integer institucionOrdenante;
	
	@Column(name = "institucion_beneficiaria", nullable = false)
	private Integer institucionBeneficiaria;
	
	@Column(name = "clave_rastreo", nullable = false)
	private String claveRastreo;
	
	@Column(name = "monto", nullable = false)
	private Double monto;
	
	@Column(name = "nombre_ordenante", nullable = false)
	private String nombreOrdenante;
	
	@Column(name = "cuenta_ordenante", nullable = false)
	private String cuentaOrdenante;
	
	@Column(name = "nombre_beneficiario", nullable = false)
	private String nombreBeneficiario;
	
	@Column(name = "tipo_cuenta_beneficiario", nullable = false)
	private Integer tipoCuentaBeneficiario;
	
	@Column(name = "cuenta_beneficiario", nullable = false)
	private String cuentaBeneficiario;
	
	@Column(name = "rfc_curp_beneficiario", nullable = false)
	private String rfcCurpBeneficiario;
	
	@Column(name = "concepto_pago", nullable = false)
	private String conceptoPago;
	
	@Column(name = "referencia_numerica", nullable = false)
	private String referenciaNumerica;
	
	@Column(name = "empresa", nullable = false)
	private String empresa;
	
	@Column(name = "id_previvale_card_stock", nullable = false)
	private Long idPrevivaleCardStock;
	
}
