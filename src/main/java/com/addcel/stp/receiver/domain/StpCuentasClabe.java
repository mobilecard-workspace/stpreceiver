/**
 * @author Victor Ramirez
 */

package com.addcel.stp.receiver.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "stp_cuentas_clabe")
public class StpCuentasClabe {

	@Id
	@Column(name = "id_stp_cuentas_clabe", nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long idStpCuentasClabe;
	
	@Column(name = "clabe", nullable = false, length = 18, unique = true)
	private String clabe;
	
	@Column(name = "institucion", nullable = false)
	private Integer institucion;
	
	@Column(name = "plaza", nullable = false)
	private Integer plaza;
	
	@Column(name = "cuenta", nullable = false, length = 11)
	private String cuenta;
	
	@Column(name = "dv", nullable = false)
	private Integer dv;
	
	@Column(name = "activa", nullable = false)
	private Integer activa;
	
}
