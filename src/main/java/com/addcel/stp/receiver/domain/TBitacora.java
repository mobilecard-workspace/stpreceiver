/**
 * @author Victor Ramirez
 */

package com.addcel.stp.receiver.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_bitacora")
public class TBitacora {

	@Id
	@Column(name = "id_bitacora", nullable = false, unique = true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long idBitacora;
	
	@Column(name = "id_usuario", nullable = false)
	private Long idUsuario;
	
	@Column(name = "id_aplicacion", nullable = true)
	private Integer idAplicacion;
	
	@Column(name = "id_proveedor", nullable = true)
	private Integer idProveedor;
	
	@Column(name = "id_producto", nullable = true)
	private Integer idProducto;
	
	@Column(name = "idioma", nullable = true)
	private String idioma;
	
	@Column(name = "bit_hora", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date bitHora;
	
	@Column(name = "bit_fecha", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date bitFecha;
	
	@Column(name = "bit_cargo", nullable = true)
	private BigDecimal bitCargo;
	
	@Column(name = "bit_concepto", nullable = true)
	private String bitConcepto;
	
	@Column(name = "bit_comision", nullable = true)
	private BigDecimal bitComision;
	
	@Column(name = "bit_ticket", nullable = true)
	private String bitTicket;
	
	@Column(name = "bit_no_autorizacion", nullable = true)
	private String bitNoAutorizacion;
	
	@Column(name = "bit_card_id", nullable = true)
	private Integer bitCardId;
	
	@Column(name = "bit_status", nullable = true)
	private Integer bitStatus;
	
	@Column(name = "tarjeta_compra", nullable = true)
	private String tarjetaCompra;
	
}
