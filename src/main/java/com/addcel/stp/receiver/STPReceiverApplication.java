/**
 * @author Victor Ramirez
 */

package com.addcel.stp.receiver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class STPReceiverApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(STPReceiverApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(STPReceiverApplication.class, args);
	}
	
}
