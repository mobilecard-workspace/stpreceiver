/**
 * @author Victor Ramirez
 */

package com.addcel.stp.receiver.controller;

import javax.validation.ConstraintViolationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.addcel.stp.receiver.request.CambioEstadoRequest;
import com.addcel.stp.receiver.response.ApiResponse;
import com.addcel.stp.receiver.service.CambioOrdenService;
import com.fasterxml.uuid.Generators;

@RestController
@RequestMapping("/api")
public class CambioEstadoController {
	
	@Autowired
	private CambioOrdenService cambioOrdenServ;

	private static final Logger LOGGER = LogManager.getLogger(CambioEstadoController.class);
	
	@PostMapping(value = "/cambioEstado", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Object cambioEstado(@RequestBody CambioEstadoRequest cambioEstadoReq) {
		ThreadContext.put("sessionID", String.valueOf(Generators.timeBasedGenerator().generate().timestamp()));
		ApiResponse resp = new ApiResponse();
		
		LOGGER.info("Recibiendo el estado de la orden: " + cambioEstadoReq.getId());
		
		resp = cambioOrdenServ.cambiarEstado(cambioEstadoReq);
		
		LOGGER.debug("Respuesta enviada al cliente: " + resp.toString());
		ThreadContext.clearAll();
		
		return resp;
	}
	
	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	ResponseEntity<String> handleConstraintViolationException(ConstraintViolationException e) {
		LOGGER.error("Uno o mas parametros no fueron recibidos correctamente");
		return new ResponseEntity<>("Error en los parametros: " + e.getMessage(), HttpStatus.BAD_REQUEST);
	}
	
}
