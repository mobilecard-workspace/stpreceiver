/**
 * @author Victor Ramirez
 */

package com.addcel.stp.receiver.ws.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AgregarDineroRequest {

	private Long idUsuario;
	private Long idEstablecimiento;
	private String tarjeta;
	private Double cantidad;
	
}
