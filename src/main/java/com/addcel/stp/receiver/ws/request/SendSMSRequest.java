/**
 * @author Victor Ramirez
 */

package com.addcel.stp.receiver.ws.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SendSMSRequest {

	private String idUsuario;
	private String idMensaje;
	private String numeroCelular;
	private SMSParams params;
	
}
