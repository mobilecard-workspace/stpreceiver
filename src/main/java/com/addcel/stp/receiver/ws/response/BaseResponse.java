/**
 * @author Victor Ramirez
 */

package com.addcel.stp.receiver.ws.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseResponse {

	private int code;
	private String message;
	
}
