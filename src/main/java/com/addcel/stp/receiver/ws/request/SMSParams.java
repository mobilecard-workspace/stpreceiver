/**
 * @author Victor Ramirez
 */

package com.addcel.stp.receiver.ws.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SMSParams {

	@JsonProperty("<nombre>")
	private String nombre;
	
	@JsonProperty("<cuenta>")
	private String cuenta;
	
	@JsonProperty("<monto>")
	private String monto;
	
}
