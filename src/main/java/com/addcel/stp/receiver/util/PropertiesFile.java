/**
 * @author Victor Ramirez
 */

package com.addcel.stp.receiver.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@PropertySource("classpath:application.properties")
@Data
public class PropertiesFile {
	
	@Value("${previvale.service.url}")
	private String previvaleServiceUrl;
	
	@Value("${previvale.service.username}")
	private String previvaleServiceUsername;
	
	@Value("${previvale.service.password}")
	private String previvaleServicePassword;
	
	@Value("${sms.service.url}")
	private String smsServiceUrl;
	
	@Value("${bitacora.concepto}")
	private String bitacoraConcepto;
	
	@Value("${bitacora.id.proveedor}")
	private int bitacoraIdProveedor;
	
	@Value("${bitacora.id.producto}")
	private int bitacoraIdProducto;
	
	@Value("${bitacora.status}")
	private int bitacoraStatus;
	
	@Value("${bitacora.id.aplicacion}")
	private int bitacoraIdAplicacion;
	
	@Value("${bitacora.idioma}")
	private String bitacoraIdioma;
	
	@Value("${stp.clabe.maestra}")
	private String stpClabeMaestra;
	
}
