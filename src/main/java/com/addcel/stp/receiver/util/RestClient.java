/**
 * @author Victor Ramirez
 */

package com.addcel.stp.receiver.util;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Base64;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.addcel.stp.receiver.constants.MobileCardConstants;
import com.addcel.stp.receiver.ws.response.BaseResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class RestClient {
	
	private static final Logger LOGGER = LogManager.getLogger(RestClient.class);
	
	@Autowired
	private PropertiesFile propsFile;

	private HttpHeaders addHeadersPV() {
		LOGGER.debug("Agregando headers para API de PreviVale");
		String basicAuth = propsFile.getPrevivaleServiceUsername() + ":" + propsFile.getPrevivaleServicePassword();
		basicAuth = Base64.getEncoder().encodeToString(basicAuth.getBytes());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Basic " + basicAuth);
		
		return headers;
	}
	
	private HttpHeaders addHeadersSMS() {
		LOGGER.debug("Agregando headers para API de SMS");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		return headers;
	}
	
	/**
	 * Metodo para consumir una API REST
	 * @param uri url del servicio rest
	 * @param requestBody body de la peticion
	 * @param rest bandera para saber cual api rest se consume
	 * @return
	 * @throws Exception
	 */
	public BaseResponse createRequest(String uri, Object requestBody, int rest) throws Exception {
		BaseResponse response = null;
		HttpHeaders headers = null;
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
		ObjectMapper mapper = new ObjectMapper();
		String body = mapper.writeValueAsString(requestBody);
		
		if(rest == MobileCardConstants.REST_PV) {
			headers = this.addHeadersPV();
		} else if(rest == MobileCardConstants.REST_SMS) {
			headers = this.addHeadersSMS();
		}
		
		HttpEntity<String> entity = new HttpEntity<String>(body, headers);
		
		LOGGER.debug("Enviando peticion a la url: " + uri);
		LOGGER.debug("Body Request: " + entity.getBody());
		
		ResponseEntity<BaseResponse> result = restTemplate.exchange(uri, HttpMethod.POST, entity, BaseResponse.class);
		response = result.getBody();
		
		LOGGER.debug("Status Code: " + result.getStatusCode());
		LOGGER.debug("Body Response: " + result.getBody());
		
		return response;
	}
	
}
