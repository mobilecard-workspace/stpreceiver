/**
 * @author Victor Ramirez
 */

package com.addcel.stp.receiver.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.addcel.stp.receiver.domain.PreviValeCardStock;

public interface PreviValeCardStockRepository extends CrudRepository<PreviValeCardStock, Long> {
	
	public PreviValeCardStock findByIdStpCuentasClabe(@Param("idStpCuentasClabe") long idStpCuentasClabe);
	
}
