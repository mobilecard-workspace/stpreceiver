/**
 * @author Victor Ramirez
 */

package com.addcel.stp.receiver.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.addcel.stp.receiver.domain.StpTransaccionesDispersion;

public interface StpTransaccionesDispersionRepository extends CrudRepository<StpTransaccionesDispersion, Long> {

	public StpTransaccionesDispersion findByIdOrden(@Param("id_orden") long id_orden);
	
	@SuppressWarnings("unchecked")
	public StpTransaccionesDispersion save(StpTransaccionesDispersion stpTransaccionesDispersion);
	
}
