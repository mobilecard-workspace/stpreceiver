/**
 * @author Victor Ramirez
 */

package com.addcel.stp.receiver.repository;

import org.springframework.data.repository.CrudRepository;

import com.addcel.stp.receiver.domain.StpTransaccionesCobranza;

public interface StpTransaccionesCobranzaRepository extends CrudRepository<StpTransaccionesCobranza, Long> {

	@SuppressWarnings("unchecked")
	public StpTransaccionesCobranza save(StpTransaccionesCobranza stpTransaccionesCobranza);
	
}
