/**
 * @author Victor Ramirez
 */

package com.addcel.stp.receiver.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.addcel.stp.receiver.domain.StpCuentasClabe;

public interface StpCuentasClabeRepository extends CrudRepository<StpCuentasClabe, Long> {

	public StpCuentasClabe findByClabe(@Param("clabe") String clabe);
	
}
