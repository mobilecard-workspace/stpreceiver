/**
 * @author Victor Ramirez
 */

package com.addcel.stp.receiver.repository;

import org.springframework.data.repository.CrudRepository;

import com.addcel.stp.receiver.domain.TBitacora;

public interface TBitacorasRepository extends CrudRepository<TBitacora, Long> {
		
	@SuppressWarnings("unchecked")
	public TBitacora save(TBitacora bitacora);
	
}
